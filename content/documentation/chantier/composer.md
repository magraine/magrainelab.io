+++
title = "Composer"
description = "Savoir dépendre des autres"
date = 2023-02-07
updated = 2023-02-07
weight = 20
+++

C’est un bon chantier de savoir quoi utisiser, pourquoi et comment.
De même c’est un autre travail de transformer notre code SPIP pour le découper en modules
installables facilement via Composer. 

## Utiliser des librairies tierces

Lesquelles et pourquoi ?

- Logger (SpipLog minimal à créer ou Monolog)
- Conteneur de service, et/ou Conteneur de dépendance (DI) avec autowiring
- Implémentation de Conteneur
- Implémentation de Cache
- Request / Response / ServerRequest 
- Middleware
- Requetage HTTP (Guzzle ou autre)
- ...

## Charger nos librairies avec Composer

- Découper certaines parties du code SPIP pour le rendre plus autonome en librairie
- Charger ces librairies SPIP avec Composer.

## Charger nos plugins avec Composer

- Nécessite, si on conserve un répertoire plugins/ un plugin Composer en typant les composer.json de nos plugins

## Problèmes avec la forge Gitea

- Pas adaptée pour intéragir avec Packagist.