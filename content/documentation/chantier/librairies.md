+++
title = "Librairies intéressantes"
description = "Un panel des possibles"
date = 2023-02-07
updated = 2023-02-07
weight = 40
+++

**Note:** Article à réactualiser.


Liste assez complete : [Awesome-php](https://github.com/ziadoz/awesome-php), mais plus vraiment actualisé


## Structures

### Tableaux

- [lstrojny/functional-php](https://github.com/lstrojny/functional-php/blob/master/docs/functional-php.md)
- [cucur/chain](https://github.com/cocur/chain)
- [doctrine/collections](https://www.doctrine-project.org/projects/doctrine-collections/en/latest/index.html)

### Structures de données

- [extension php-ds](http://docs.php.net/manual/en/book.ds.php) + polyfill php-ds
- [SPL](http://php.net/manual/fr/spl.datastructures.php)

### Pipelines

- [sanmai/pipeline](https://packagist.org/packages/sanmai/pipeline)
- [league/pipeline](https://github.com/thephpleague/pipeline)

### Dates

- [sesbot/carbon](https://carbon.nesbot.com/)


## Disque

### Fichiers

- [symfony/finder](https://symfony.com/doc/current/components/finder.html)

### Logs

- [monolog/monolog](https://github.com/Seldaek/monolog)

### Lock

- [symfony/lock](https://symfony.com/doc/current/components/lock.html)

	

## Exceptions / Assertions / Debug

### Assertions

- [webmozart/assert](https://github.com/webmozart/assert)


## Debug

- [symfony/debug](https://symfony.com/doc/current/components/debug.html)



## Applications

### Routeurs

- [symfony/routeur](https://symfony.com/doc/current/routing.html) (maintenant plus rapide que FastRoute, en 4.1)
- [league/route](http://route.thephpleague.com/) (utilise Fastroute)

### Requétage HTTP

- [guzzlehttp/guzzle](http://docs.guzzlephp.org/en/stable/)
- [nyholm/psr7](https://github.com/Nyholm/psr7)

### Configurations

- [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv)
- [symfony/Dotenv](https://symfony.com/doc/current/components/dotenv.html)


## Extensibilité

### Macros

- [Trait macro dans Carbon](https://github.com/briannesbitt/Carbon/blob/master/src/Carbon/Traits/Macro.php)
- [Trait macroable dans Laravel](https://github.com/laravel/framework/blob/5.2/src/Illuminate/Support/Traits/Macroable.php)
- [spatie/macroable](https://github.com/spatie/macroable)


## Bus / Messages

- [league/tactician](http://tactician.thephpleague.com/)
- [symfony/messager](https://symfony.com/doc/current/components/messenger.html)




# Concepts de programmation

## [Reproductible builds](https://symfony.com/blog/new-in-symfony-reproducible-builds)

Reproducible builds are a set of software development practices that create "a verifiable path from human readable source code to the binary code used by computers". In other words, if you don't change the source code, the compilation result should always be exactly the same.



# Idée pour twig

## Extensions

- [twig-list-loop](https://github.com/aaronadal/twig-list-loop)
- [qeep-pro/twig-tree-tag](https://github.com/QEEP-Pro/twig-tree-tag) : un for récursif…
- [magdkudama/twig-highlight](https://github.com/magdkudama/twig-highlight) : coloration de code avec `{% highlight php %}`
- [dstone/twig-optimizations](https://github.com/superdav42/TwigOptimizations/) : il y a un node visitor sur les recherches de valeurs `{{ truc.nom }}`

- Le tag `{% setcontent %}` de Bolt, permet de récupérer des données en BDD ! [content-fetching](https://docs.bolt.cm/3.6/templating/content-fetching) ; et les différents ajouts de Bolt à Twig : [twig functionality](https://docs.bolt.cm/3.6/templating/twig-functionality)


## Compilo


- [explication compilateur](https://stackoverflow.com/questions/26170727/how-to-create-a-twig-custom-tag-that-executes-a-callback)



# Autour de SQL

## Portabilité Mysql / Sqlite / Postgres

- [quelques différences](https://evertpot.com/writing-sql-for-postgres-mysql-sqlite/)
- [aura/sqlquery](https://github.com/auraphp/Aura.SqlQuery/blob/3.x/docs/index.md) Abstraction SQL (mysql, postgres, sqlite, sqlserver)
- [aura/sql](https://github.com/auraphp/Aura.Sql/blob/3.x/docs/index.md) Extension de Pdo (timer, yield...)
- [opis/database](https://www.opis.io/database/4.x/) Abstraction & Fluent ; semble bien.
- [doctrine/dbal](https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/index.html) Abstraction
- [Cycle ORM](https://cycle-orm.dev)


