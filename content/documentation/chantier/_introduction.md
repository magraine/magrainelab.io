+++
title = "Introduction"
date = 2023-02-07
updated = 2023-02-07
weight = 5
template = "page-introduction.html"
+++

## Quel chantier pour SPIP ?

Est-il souhaitable et possible de transformer SPIP ? Vers où aller dans ce cas ? Pourquoi ? Comment ? 

Y a du boulot et un gros chantier pour aller vers un code plus moderne.
Le plus délicat étant probablement d’assurer une migration sans trop de difficulté des sites existants.
