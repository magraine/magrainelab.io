+++
title = "Organisation des dossiers"
description = "Quelle organisation des répertoires ?"
date = 2023-02-07
updated = 2023-02-07
weight = 30
+++

## Structure de base

	.
	├── config
	├── docs
	├── public (ou www ou sites)
	├── src
	├── tests
	└── vendor

## Structure SPIP

### Modèle simple

	.
	├── composer.json
	├── docs
	├── index.php (dirige vers sites/defaut/public/index.php)
	├── site
	│   ├── config
	│   ├── public
	│   │   ├── assets (ancien local)
	│   │   ├── data (ancien IMG)
	│   │   ├── htaccess.txt (éventuellement)
	│   │   └── index.php
	│   └── tmp
	├── src (anciens équivalents à ecrire/)
	├── tests
	└── vendor

### Modèle mutualisation

	.
	├── composer.json
	├── docs
	├── index.php (dirige vers sites/defaut/public/index.php)
	├── sites
	│   ├── X
	│   ├── Y
	│   ├── defaut
	│   │   ├── config
	│   │   ├── public
	│   │   │   ├── assets (ancien local)
	│   │   │   ├── data (ancien IMG)
	│   │   │   ├── htaccess.txt (éventuellement)
	│   │   │   └── index.php
	│   │   └── tmp
	│   └── demo.spip.net
	├── src (anciens équivalents à ecrire/)
	├── tests
	└── vendor

Les vhost font pointer les sites sur le répertoire `public` tel que dans `sites/defaut/public`.

