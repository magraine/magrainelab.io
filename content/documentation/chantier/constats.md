+++
title = "Constats généraux"
description = "Quel est l’état d’internet, de PHP, de SPIP ?"
date = 2023-02-07
updated = 2023-02-07
weight = 10
+++

## Autour de SPIP

- son usage diminue doucement, notamment parce que d’autres outils pertinents ou pratiques existent ; qu’il n’y a pas d’évolution flagrante de notre côté au fil du temps. Des logiciels (Wordpress, entre autres) aux moyens conséquents ont su capter par leur facilité de mise en œuvre.
- sa communauté s’étiole aussi doucement (moins de présence d’utilisatrices et utilisateurs sur les forums, salons de discussions, de participation et membres actifs sur la zone ou les plugins), notamment du à un vieillisement de ses actrices & acteurs. La jeunesse qui code aura tendence à aller vers des outils au code plus moderne.
- le nombre de sites SPIP diminue légèrement également.

## Autour de PHP

[PHP](https://www.php.net/) est toujours actif et très utilisé ; sa communauté s’est trouvé des méthodologies (structure des répertoires, namespace, Composer, PSR, dépendances) donnant des librairies plus facilement interchangeables et réutilisables.

Cependant d’autres langages aussi se développent brillament à grand pas ; 
Citons entre autres [NodeJs](https://nodejs.org/fr/), [Rust](https://www.rust-lang.org/fr) ou [Go](https://go.dev/).

## Autour du Web

Le web s’est professionnalisé et les métiers sectorisés ; il n’y plus vraiment de personne à tout faire, mais différents métiers (graphistes, designeuses, développeuses, intégratrices, etc) bien plus spécialisés dans leur domaine. 

La publication est devenue très facile, avec de nombreux outils ou sites disponibles, plus ou moins heureux (wix, tumblr, facebook, twitter, snapchat, youtube, tik tok, instagram…).

La contrepartie évidente étant la perte / vol des données lorsque des structures utilisent les GAFAM pour publier. Cependant d’autres outils libres, comme Wordpress ont su apporter des solutions de publication simples pour les structures.

L’intérêt de SPIP est quasi nul actuellement pour les individus ou petites associations : 
  - une page Facebook ou Instagram suffit… 
  - et sinon, un site statique compilé fait parfaitement l’affaire, en offrant des performances forcément maximales 

Il reste cependant pertinent pour des sites un peu plus conséquents, où la collaboration et publication est active ; Journaux, associations, communes, collectivités, collèges, entreprises…

Les mobiles et leurs applications ont modifié de façon conséquente le web et les sites web. Notamment en utilisant JSON et HTTP pour intéragir avec l’application… Certains outils de publication ne fonctionnent que par API json d’ailleurs.

## Des questions politiques

Le web est victime d’une abondance et prolifération d’informations instantannées ; de contenus qui cherchent uniquement à capter l’attention. Des textes très courts aguicheurs ou crispants (twitter) aux vidéos courtes chocs, sexys, originales ou provoquantes (tik tok, shorts youtube) sont mis en avant. D’une part le cerveau en est friand, et d’autre part, cela limite la réflexion de fond et tend à créer des bulles sociales très clivées.

Les robots et trolls ont pollué tellement les forums et sont tellement la plaie à gérer (tout comme la gestion de mails), que quasiment plus personne ne met d’espace commentaires sur son site. Il est par conséquent difficile d’avoir des outils permettant des échanges constructifs sur le web, sans faire une censure drastiques des commentaires (insultes, sexismes, racismes, homophobie, bots de SEO ou trolls, ...) épuisante et chronophage. C’est dommage et frustrant.

Quel internet voulons-nous ?

