+++
title = "Conventional commits"
description = "Comment écrire ses logs de commits ?"
date = 2023-02-06
updated = 2023-02-06
weight = 20
+++

## Kesaquo ?

> Convention légère pour des messages de commit propre

Ça sert à rédiger des messages de commits plus facilement identifiables par les humains et les machines.
À partir du moment évidemment ou le contenu du commit est lui aussi atomique.


## Les types (chez SPIP)

- `build`: Modifie un script de compilation du programme ou modifie des dépendances externes.
- `change`: Modifie l’implémentation d’une fonctionnalité — peut modifier des signatures de fonctions (≠ refactor)
- `chore`: Travail de fond n’altérant pas le code (sortir une version, regénérer du code compilé).
- `ci`: Relatif à l’intégration continue.
- `deprecate`: Déprécier (sans l’enlever) une fonctionnalité.
- `docs`: Relatif à la documentation (docs/, readme, changelog)
- `feat`: Ajoute une fonctionnalité.
- `fix`: Corrige un problème.
- `localize`: Relatif aux traductions et chaînes de langue (ou `i18n`, ou `l10n` à définir).
- `perf`: Améliore la preformance d’algorithme ou du programme.
- `refactor`: Réécriture de code, sans en modifier l’implémentation.
- `remove`: Suppression de code ou de fonctionnalité (déprécié auparavant)
- `revert`: Annule une modification précédente (commit) 
- `security`: Relatif à la sécurité
- `style`: Relatif aux règles d’écriture du code (Coding Standard)
- `test`: Relatifs aux tests du logiciel


## Références

- [Conventional Commits - v1](https://www.conventionalcommits.org/fr/v1.0.0/)
- [Conventional Commits: a better way](https://medium.com/neudesic-innovation/conventional-commits-a-better-way-78d6785c2e08)
- [Discussion chez SPIP](https://git.spip.net/spip/spip/issues/5208)