+++
title = "Keep a Changelog"
description = "Comment écrire son Changelog ?"
date = 2023-02-06
update = 2023-02-06
weight = 30
+++

## Kesaquo ?

> Ne laissez pas vos amis utiliser les logs git comme changelogs

Ça sert à rédiger plus correctement un fichier `CHANGELOG.md`

## Principes Directeurs

- Les changelogs sont pour les êtres humains, pas les machines.
- Il doit y avoir une section pour chaque version.
- **Les changements similaires doivent être groupés.**
- Les versions et sections doivent être liables.
- La version la plus récente se situe en haut du fichier.
- La date de publication de chaque version est indiquée (exemple: 2023-01-28).
- Indiquer si le projet respecte le Versionnage Sémantique.


## Types de changements

- `Added` pour les nouvelles fonctionnalités.
- `Changed` pour les changements aux fonctionnalités préexistantes.
- `Deprecated` pour les fonctionnalités qui seront bientôt supprimées.
- `Removed` pour les fonctionnalités désormais supprimées.
- `Fixed` pour les corrections de bugs.
- `Security` en cas de vulnérabilités.


## Référence

- [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)