+++
title = "Composer"
description = "Comment écrire le Composer.json ?"
date = 2023-02-06
update = 2023-02-06
weight = 40
+++

## Pour quoi faire ?

Le fichier `composer.json` permet d’indiquer (à l’instar de `paquet.xml` de SPIP) un nom, une description
des dépendances à charger pour une librairie donnée.

Il peut recevoir aussi des alias de commandes, des paramètres de configuration.

## Normaliser son écriture

On peut utiliser `composer normalize` fournit par [ergebnis/composer-normalize](https://github.com/ergebnis/composer-normalize).

## Normaliser le nom des scripts aliasés

Se référer à [php-pds/composer-script-names](https://github.com/php-pds/composer-script-names)

- `test`: Exécuter les tests
- `test-coverage`: Exécuter les tests, avec analyse de couverture
- `test-*`: Exécuter les tests, avec variante de configuration
- `cs-fix`: Corriger les coding standards
- `analyse` ou `analyze`: Exécuter un analyseur statique (exemple: php-stan)
- `check`: Exécuter plusieurs scripts de QA (exemple: tests, cs, analyse)
