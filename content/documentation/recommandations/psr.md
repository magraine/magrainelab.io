+++
title = "PHP (PSR & PER)"
description = "Comment écrire son code PHP ?"
date = 2023-02-06
update = 2023-02-06
weight = 10
+++

## Kesaquo ?

> Moving PHP forward through collaboration and standards.

- **PSR**: PHP Standard Recommendation
- **PER**: PHP Evolving Recommendation

## Coding Standard

Les coding standards définissent comment écrire un code PHP (sauts de ligne, indentations, espaces...) et permettent
d’homogénéiser les différents fichiers de son projet. Utiliser les recommandations rend plus facile la configuration
des outils de CS (tel que phpcs).

### Quelques outils

- [php cs fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer)
- [phpcs, phpcbf](https://github.com/squizlabs/PHP_CodeSniffer)
- [ecs](https://github.com/easy-coding-standard/easy-coding-standard)
- [rector](https://github.com/rectorphp/rector)


## Interfaces

Les interfaces permettent faciliter la transition entre différentes librairies ou objets, en offrant une API fixe.
Par ailleurs les interfaces issues de PSR facilitent la compréhension en utilisant une culture commune.

### Quelques PSR

Que l’on pourrait utiliser facilement dans SPIP certainement.

- [PSR-3: Logger interface](https://www.php-fig.org/psr/psr-3/): Pour les logs
- [PSR-6]((https://www.php-fig.org/psr/psr-6/)) et [PSR-16]((https://www.php-fig.org/psr/psr-16/)) — Caching interface
- [PSR-11: Container interface ](https://www.php-fig.org/psr/psr-11/): pour des conteneurs de services ou d’injection de dépendances

Mais bien d’autres seraient aussi utiles (Message, Events, Middleware, …)

## Références

- [PHP-FIG](https://www.php-fig.org/)
