+++
title = "Introduction"
date = 2023-02-06
updated = 2023-02-06
weight = 5
template = "page-introduction.html"
+++

## Pourquoi fixer des règles ?

Pour mieux collaborer, et faciliter l’entraide et l’apprentissage en utilisant une culture commune.
L’écosystème PHP et Javascript sont nos amis ! Essayons de faire au mieux avec leurs conventions.

